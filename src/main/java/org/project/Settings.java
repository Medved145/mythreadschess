package org.project;

import org.project.util.ChessboardPrinter.ViewType;
import org.project.util.Color;

public class Settings {

    public static ViewType viewType = ViewType.TEXT;
    public static String whitePiecesColor = Color.GREEN;
    public static String blackPiecesColor = Color.RED;
    public static String defaultColor = Color.RESET;

    public static boolean endlessGame = false;
    public static int millisecondsToEndGame = 50000;
    public static boolean turnDelay = true;
    public static int  turnTime = 500;
}
