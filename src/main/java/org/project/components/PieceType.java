package org.project.components;

public enum PieceType {
    ROOK(true, true),
    KNIGHT(false, true),
    BISHOP(false, true),
    QUEEN(false, true),
    PAWN(true, false),
    KING(true, false);

    private final boolean standOnStart;
    private final boolean movesMoreThanOneField;

    PieceType(boolean standOnStart, boolean movesMoreThanOneField) {
        this.standOnStart = standOnStart;
        this.movesMoreThanOneField = movesMoreThanOneField;
    }

    public boolean isStandOnStart() {
        return standOnStart;
    }

    public boolean isMovesMoreThanOneField() {
        return movesMoreThanOneField;
    }
}
