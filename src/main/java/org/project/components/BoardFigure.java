package org.project.components;

import static org.project.Settings.*;

public enum BoardFigure {
    WHITE_KING('K', '♔', whitePiecesColor),
    WHITE_QUEEN('Q', '♕', whitePiecesColor),
    WHITE_ROOK('R', '♖', whitePiecesColor),
    WHITE_BISHOP('B', '♗', whitePiecesColor),
    WHITE_KNIGHT('N', '♘', whitePiecesColor),
    WHITE_PAWN('P', '♙', whitePiecesColor),
    BLACK_KING('k', '♚', blackPiecesColor),
    BLACK_QUEEN('q', '♛',blackPiecesColor),
    BLACK_ROOK('r', '♜', blackPiecesColor),
    BLACK_BISHOP('b', '♝', blackPiecesColor),
    BLACK_KNIGHT('n', '♞', blackPiecesColor),
    BLACK_PAWN('p', '♟', blackPiecesColor),
    EMPTY_PAWN(' ', ' ', defaultColor);


    private final char figureSymbol;
    private final char utf8Figure;
    private final String color;

    BoardFigure(char figureSymbol, char utf8Figure, String color) {
        this.figureSymbol = figureSymbol;
        this.utf8Figure = utf8Figure;
        this.color = color;
    }

    public char getFigureSymbol() {
        return figureSymbol;
    }

    public char getUtf8Figure() {
        return utf8Figure;
    }

    public String getColor() {
        return color;
    }
}
