package org.project.components;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Chessboard {
    private Piece[][] board;
    private static Chessboard instance;

    private Chessboard() {
        boardInitialization();
    }

    public Piece[][] getBoard() {
        return board;
    }

    public static Chessboard getInstance() {
        if (instance == null) {
            instance = new Chessboard();
        }
        return instance;
    }

    private void boardInitialization() {
        board = new Piece[8][8];
        for (int xAxis = 0; xAxis < 8; xAxis++) {
            for (int yAxis = 0; yAxis < 8; yAxis++) {
                int index = xAxis * 8 + yAxis;
                BoardField position = BoardField.getByIndex(index);
                if (xAxis > 1 && xAxis < 6) {
                    board[xAxis][yAxis] = null;
                } else {
                    boolean white = xAxis < 2;
                    board[xAxis][yAxis] = new Piece(getActualPawnType(index), white, position);
                }
            }
        }
    }

    public boolean pieceIsContained(Piece piece) {
        for (Piece[] pieces : board) {
            for (Piece single : pieces) {
                if (piece.equals(single)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void reinitializeBoard() {
        boardInitialization();
    }

    public void clearBoard() {
        for (Piece[] pieces : board) {
            Arrays.fill(pieces, null);
        }
    }

    public void dislocatePiece(@NotNull Piece piece, BoardField newPosition) {
        String pieceColor = piece.isWhite() ? "WHITE " : "BLACK ";
        System.out.println(pieceColor + piece.getType() + " " + piece.getPosition() + "-" + newPosition);
        BoardField oldPosition = piece.getPosition();
        piece.setPosition(newPosition);
        if (piece.isStandOnStart()) {
            piece.setStandOnStart(false);
        }
        if (piece.getType() == PieceType.PAWN) {
            pawnUpgrade(piece);
        }
        board[newPosition.getY()][newPosition.getX()] = piece;
        board[oldPosition.getY()][oldPosition.getX()] = null;
    }

    private void pawnUpgrade(@NotNull Piece piece) {
        int index = ThreadLocalRandom.current().nextInt(4);
        Direction direction = piece.isWhite() ? Direction.TO_UP : Direction.TO_DOWN;
        if (BoardField.getNewPosition(piece.getPosition(), direction) == null) {
            piece.setType(PieceType.values()[index]);
            String pawnColor = piece.isWhite() ? "WHITE " : "BLACK ";
            System.out.println(pawnColor + "PAWN now is " + pawnColor + piece.getType());
        }
    }

    private PieceType getActualPawnType(int index) {
        switch (index) {
            case 0:
            case 7:
            case 56:
            case 63:
                return PieceType.ROOK;
            case 1:
            case 6:
            case 57:
            case 62:
                return PieceType.KNIGHT;
            case 2:
            case 5:
            case 58:
            case 61:
                return PieceType.BISHOP;
            case 3:
            case 59:
                return PieceType.QUEEN;
            case 4:
            case 60:
                return PieceType.KING;
            default:
                return PieceType.PAWN;
        }
    }
}
