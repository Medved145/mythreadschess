package org.project.components;

import static org.project.components.BoardField.*;

public enum Direction {
    TO_UP(8, new BoardField[]{a8, b8, c8, d8, e8, f8, g8, h8}),
    TO_DOWN(-8, new BoardField[]{a1, b1, c1, d1, e1, f1, g1, h1} ),
    TO_LEFT(-1, new BoardField[]{a1, a2, a3, a4, a5, a6, a7, a8}),
    TO_RIGHT(1, new BoardField[]{h1, h2, h3, h4, h5, h6, h7, h8}),
    TO_UP_LEFT(TO_UP.getCoefficent() + TO_LEFT.getCoefficent(), new BoardField[]{a8, b8, c8, d8, e8, f8, g8, h8, a1, a2, a3, a4, a5, a6, a7, a8}),
    TO_UP_RIGHT(TO_UP.getCoefficent() + TO_RIGHT.getCoefficent(), new BoardField[]{a8, b8, c8, d8, e8, f8, g8, h8, h1, h2, h3, h4, h5, h6, h7, h8}),
    TO_DOWN_LEFT(TO_DOWN.getCoefficent() + TO_LEFT.getCoefficent(), new BoardField[]{a1, b1, c1, d1, e1, f1, g1, h1, a1, a2, a3, a4, a5, a6, a7, a8}),
    TO_DOWN_RIGHT(TO_DOWN.getCoefficent() + TO_RIGHT.getCoefficent(), new BoardField[]{a1, b1, c1, d1, e1, f1, g1, h1, h1, h2, h3, h4, h5, h6, h7, h8});

    private final int coefficent;
    private final BoardField[] edge;

    Direction(int coefficent, BoardField[] edge)
    {
        this.coefficent = coefficent;
        this.edge = edge;
    }

    public int getCoefficent() {
        return coefficent;
    }

    public BoardField[] getEdge() {
        return edge;
    }
}
