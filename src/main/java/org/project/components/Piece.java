package org.project.components;

import org.jetbrains.annotations.NotNull;
import org.project.game.ChessPieceMovementRules;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class Piece implements Callable<Map<Piece, List<BoardField>>> {

    private PieceType type;
    private boolean white;
    private BoardField position;
    private boolean standOnStart;

    public Piece(PieceType type, boolean white, BoardField position) {
        this.type = type;
        this.white = white;
        this.position = position;
        standOnStart = type != null && type.isStandOnStart();
    }

    public PieceType getType() {
        return type;
    }

    public void setType(PieceType type) {
        this.type = type;
    }

    public boolean isWhite() {
        return white;
    }

    public void setWhite(boolean white) {
        this.white = white;
    }

    public BoardField getPosition() {
        return position;
    }

    public void setPosition(BoardField position) {
        this.position = position;
    }

    public boolean isStandOnStart() {
        return standOnStart;
    }

    public void setStandOnStart(boolean standOnStart) {
        this.standOnStart = standOnStart;
    }

    @Override
    public Map<Piece, List<BoardField>> call() {
        Map<Piece, List<BoardField>> result = new HashMap<>();
        Piece[][] board = Chessboard.getInstance().getBoard();
        List<BoardField> ways = ChessPieceMovementRules.getPieceWays(this, board);
        printWaysOnConsole(ways);
        result.put(this, ways);
        return result;
    }

    private void printWaysOnConsole(@NotNull List<BoardField> ways) {
        String pawnColor = white ? "WHITE " : "BLACK ";
        StringBuilder message = new StringBuilder(pawnColor + type + ":");
        for (BoardField field : ways) {
            message.append(" ").append(position).append("-").append(field).append(",");
        }
        System.out.println(message);
    }
}
