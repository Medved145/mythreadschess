package org.project.util;

import org.project.components.BoardFigure;
import org.project.components.Piece;

import static org.project.Settings.*;
import static org.project.components.BoardFigure.*;

public class ChessboardPrinter {

    public enum ViewType {
        TEXT, SYMBOL;
    }

    public static void printChessboard(Piece[][] board) {
        StringBuilder field = new StringBuilder("  ╔═══╤═══╤═══╤═══╤═══╤═══╤═══╤═══╗\n");
        for (int i = 7; i >= 0; i--) {
            field.append(i + 1).append(" ║ ");
            for (int j = 0; j < 8; j++) {
                char symbol;
                BoardFigure printableFigure = getPrintableFigure(board[i][j]);
                symbol = viewType.equals(ViewType.TEXT) ? printableFigure.getFigureSymbol() :
                        printableFigure.getUtf8Figure();
                String colorSymbol = printableFigure.getColor() + symbol + defaultColor;
                field.append(colorSymbol).append(j < 7 ? " │ " : " ║\n");
            }
            field.append(i > 0 ? "  ╟───┼───┼───┼───┼───┼───┼───┼───╢\n" : "  ╚═══╧═══╧═══╧═══╧═══╧═══╧═══╧═══╝\n");
        }
        field.append("    A   B   C   D   E   F   G   H");
        System.out.println(field);
    }

    private static BoardFigure getPrintableFigure(Piece piece) {
        if (piece != null) {
            switch (piece.getType()) {
                case PAWN:
                    return piece.isWhite() ? WHITE_PAWN : BLACK_PAWN;
                case ROOK:
                    return piece.isWhite() ? WHITE_ROOK : BLACK_ROOK;
                case KNIGHT:
                    return piece.isWhite() ? WHITE_KNIGHT : BLACK_KNIGHT;
                case BISHOP:
                    return piece.isWhite() ? WHITE_BISHOP : BLACK_BISHOP;
                case QUEEN:
                    return piece.isWhite() ? WHITE_QUEEN : BLACK_QUEEN;
                case KING:
                    return piece.isWhite() ? WHITE_KING : BLACK_KING;
            }
        }
        return EMPTY_PAWN;
    }
}
