package org.project;

import org.project.game.ChessGame;

import static org.project.Settings.endlessGame;
import static org.project.Settings.millisecondsToEndGame;

public class ChessApplication {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Chess");
        ChessGame chessGame = new ChessGame();
        (new Thread(chessGame)).start();
        if (!endlessGame) {
            Thread.sleep(millisecondsToEndGame);
            chessGame.stop();
        }
    }
}
