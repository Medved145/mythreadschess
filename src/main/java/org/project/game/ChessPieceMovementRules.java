package org.project.game;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.project.components.BoardField;
import org.project.components.Direction;
import org.project.components.Piece;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.project.components.Direction.*;

public class ChessPieceMovementRules {

    private static final Direction[] whitePawnDirections = {TO_UP, TO_UP_RIGHT, TO_UP_LEFT};
    private static final Direction[] blackPawnDirections = {TO_DOWN, TO_DOWN_RIGHT, TO_DOWN_LEFT};
    private static final Direction[] bishopDirections = {TO_UP_LEFT, TO_UP_RIGHT, TO_DOWN_RIGHT, TO_DOWN_LEFT};
    private static final Direction[] rookDirections = {TO_UP, TO_RIGHT, TO_DOWN, TO_LEFT};
    private static final Direction[] queenNKingDirections = {TO_UP_LEFT, TO_UP, TO_UP_RIGHT, TO_RIGHT, TO_DOWN_RIGHT,
            TO_DOWN, TO_DOWN_LEFT, TO_LEFT};

    public static List<BoardField> getPieceWays(Piece piece, Piece[][] chessPositions) {
        if (piece != null) {
            switch (piece.getType()) {
                case PAWN:
                    return getPawnMoves(piece, chessPositions);
                case KNIGHT:
                    return getKnightMoves(piece, chessPositions);
                case ROOK:
                    return getEliteMoves(piece, chessPositions, rookDirections);
                case BISHOP:
                    return getEliteMoves(piece, chessPositions, bishopDirections);
                case KING:
                case QUEEN:
                    return getEliteMoves(piece, chessPositions, queenNKingDirections);
            }
        }
        return null;
    }

    public static @NotNull List<BoardField> getPawnMoves(Piece piece, Piece[][] chessPositions) {
        List<BoardField> ways = new ArrayList<>();
        if (chessPositions != null) {
            Direction[] directions = piece.isWhite() ? whitePawnDirections : blackPawnDirections;
            BoardField position = piece.getPosition();

            // шагать вверх на 1 клетку, если возможно // +8 index position
            ways.add(getPawnWay(position, directions[0], chessPositions));
            // шагать вверх на 2 клетки, если возможно // +8*2 index position
            if (piece.isStandOnStart()) {
                if (ways.get(0) != null) {
                    ways.add(getPawnWay(ways.get(0), directions[0], chessPositions));
                }
            }
            // рубить вверх и  вправо,   если возможно // +9 index position
            ways.add(getPawnAttack(position, directions[1], piece, chessPositions));
            // рубить вверх и  влево,    если возможно // +7 index position
            ways.add(getPawnAttack(position, directions[2], piece, chessPositions));
            ways.removeIf(Objects::isNull);
        }
        return ways;
    }

    public static @NotNull List<BoardField> getKnightMoves(Piece piece, Piece[][] chessPositions) {
        List<BoardField> ways = new ArrayList<>();
        if (chessPositions != null) {
            BoardField position = piece.getPosition();
            // шагать/рубить вверх  на 2 клетки, влево  на 1, если возможно // +8*2-1 index position
            ways.add(getKnightWay(position, piece, TO_UP, TO_UP_LEFT, chessPositions));
            // шагать/рубить вверх  на 2 клетки, вправо на 1, если возможно // +8*2+1 index position
            ways.add(getKnightWay(position, piece, TO_UP, TO_UP_RIGHT, chessPositions));
            // шагать/рубить вправо на 2 клетки, вверх  на 1, если возможно // +1*2+8 index position
            ways.add(getKnightWay(position, piece, TO_RIGHT, TO_UP_RIGHT, chessPositions));
            // шагать/рубить вправо на 2 клетки, вниз   на 1, если возможно // +1*2-8  index position
            ways.add(getKnightWay(position, piece, TO_RIGHT, TO_DOWN_RIGHT, chessPositions));
            // шагать/рубить вниз   на 2 клетки, влево  на 1, если возможно // -8*2-1 index position
            ways.add(getKnightWay(position, piece, TO_DOWN, TO_DOWN_LEFT, chessPositions));
            // шагать/рубить вниз   на 2 клетки, вправо на 1, если возможно // -8*2+1 index position
            ways.add(getKnightWay(position, piece, TO_DOWN, TO_DOWN_RIGHT, chessPositions));
            // шагать/рубить влево  на 2 клетки, вверх  на 1, если возможно // -1*2+8  index position
            ways.add(getKnightWay(position, piece, TO_LEFT, TO_UP_LEFT, chessPositions));
            // шагать/рубить влево  на 2 клетки, вниз   на 1, если возможно // -1*2-8 index position
            ways.add(getKnightWay(position, piece, TO_LEFT, TO_DOWN_LEFT, chessPositions));
            ways.removeIf(Objects::isNull);
        }
        return ways;
    }

    public static @NotNull List<BoardField> getEliteMoves(Piece piece, Piece[][] chessPositions, Direction[] directions) {
        List<BoardField> ways = new ArrayList<>();
        if (chessPositions != null) {
            BoardField position = piece.getPosition();
            for (Direction direction : directions) {
                ways.addAll(getOrthogonalDiagonalWays(position, piece, direction, chessPositions));
            }
        }
        return ways;
    }

    private static @Nullable BoardField getPawnWay(BoardField position, Direction direction, Piece[][] chessPositions) {
        BoardField newPosition = BoardField.getNewPosition(position, direction);
        if (newPosition != null) {
            Piece target = chessPositions[newPosition.getY()][newPosition.getX()];
            if (target == null) {
                return newPosition;
            }
        }
        return null;
    }

    private static @Nullable BoardField getPawnAttack(BoardField position, Direction direction, Piece piece,
                                                      Piece[][] chessPositions) {
        BoardField newPosition = BoardField.getNewPosition(position, direction);
        if (newPosition != null) {
            Piece target = chessPositions[newPosition.getY()][newPosition.getX()];
            if (target != null && target.isWhite() != piece.isWhite()) {
                return newPosition;
            }
        }
        return null;
    }

    private static @Nullable BoardField getKnightWay(BoardField position, Piece piece, Direction firstMove,
                                                     Direction secondMove, Piece[][] chessPositions) {
        BoardField newPosition = BoardField.getNewPosition(position, firstMove);
        if (newPosition != null) {
            newPosition = BoardField.getNewPosition(newPosition, secondMove);
            if (newPosition != null) {
                Piece target = chessPositions[newPosition.getY()][newPosition.getX()];
                if (target == null || target.isWhite() != piece.isWhite()) {
                    return newPosition;
                }
            }
        }
        return null;
    }

    private static @NotNull List<BoardField> getOrthogonalDiagonalWays(BoardField position, Piece piece,
                                                                       Direction direction, Piece[][] chessPositions) {
        List<BoardField> ways = new ArrayList<>();
        BoardField newPosition = BoardField.getNewPosition(position, direction);
        if (newPosition != null) {
            Piece target = chessPositions[newPosition.getY()][newPosition.getX()];
            if (target == null) {
                ways.add(newPosition);
                if (piece.getType().isMovesMoreThanOneField()) {
                    ways.addAll(getOrthogonalDiagonalWays(newPosition, piece, direction, chessPositions));
                }
            } else if (target.isWhite() != piece.isWhite()) {
                ways.add(newPosition);
            }
        }
        return ways;
    }

}
