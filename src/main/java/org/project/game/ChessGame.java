package org.project.game;

import org.jetbrains.annotations.NotNull;
import org.project.components.BoardField;
import org.project.components.Chessboard;
import org.project.components.Piece;
import org.project.util.ChessboardPrinter;

import java.util.*;
import java.util.concurrent.*;

import static org.project.Settings.*;

public class ChessGame implements Runnable {

    private boolean whiteTurn;
    private boolean gameRun;

    public ChessGame() {
        whiteTurn = true;
        gameRun = true;
    }

    public void stop() {
        gameRun = false;
    }

    public void restart() {
        Chessboard.getInstance().reinitializeBoard();
        whiteTurn = true;
        gameRun = true;
    }

    @Override
    public void run() {
        ChessboardPrinter.printChessboard(Chessboard.getInstance().getBoard());
        int turnsCount = 0;
        while (gameRun) {
            List<Piece> pieces = getFigures(whiteTurn);
            if (!pieces.isEmpty()) {
                turnsCount++;
                whiteTurn = !whiteTurn;
                chooseAndMovePiece(pieces);
                ChessboardPrinter.printChessboard(Chessboard.getInstance().getBoard());
                if (turnDelay) {
                    try {
                        Thread.sleep(turnTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                gameRun = false;
            }
        }
        System.out.println("GAME OVER");
        System.out.println("Moves have been made: " + turnsCount);
    }

    private void chooseAndMovePiece(List<Piece> pieces) {
        List<Future<Map<Piece, List<BoardField>>>> piecesFutures = new ArrayList<>();
        ExecutorService executor = Executors.newFixedThreadPool(pieces.size());
        try {
            piecesFutures = executor.invokeAll(pieces);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        searchWays(piecesFutures);
        executor.shutdown();
    }

    private void searchWays(List<Future<Map<Piece, List<BoardField>>>> piecesFutures) {
        Map<Piece, List<BoardField>> waysMap = getPiecesWaysMap(piecesFutures);
        Set<Piece> canMovePieces = waysMap.keySet();
        if (!canMovePieces.isEmpty()) {
            movePiece(waysMap, canMovePieces);
        } else {
            gameRun = false;
        }
    }

    private void movePiece(Map<Piece, List<BoardField>> waysMap, Set<Piece> canMovePieces) {
        int index = canMovePieces.size() > 1 ? ThreadLocalRandom.current().nextInt(canMovePieces.size()) : 0;
        Piece chosenPiece = (Piece) canMovePieces.toArray()[index];
        List<BoardField> ways = waysMap.get(chosenPiece);
        index = ways.size() > 1 ? ThreadLocalRandom.current().nextInt(ways.size()) : 0;
        System.out.print("Chosen: ");
        Chessboard.getInstance().dislocatePiece(chosenPiece, ways.get(index));
    }

    private Map<Piece, List<BoardField>> getPiecesWaysMap(List<Future<Map<Piece, List<BoardField>>>> piecesFutures) {
        Map<Piece, List<BoardField>> resultWaysMap = new HashMap<>();
        for (Future<Map<Piece, List<BoardField>>> future : piecesFutures) {
            Map<Piece, List<BoardField>> waysMap = new HashMap<>();
            try {
                waysMap = future.get();
            } catch (InterruptedException | ExecutionException exception) {
                exception.printStackTrace();
            }
            Piece key = (Piece) waysMap.keySet().toArray()[0];
            if (!waysMap.get(key).isEmpty()) {
                resultWaysMap.putAll(waysMap);
            }
        }
        return resultWaysMap;
    }

    private @NotNull List<Piece> getFigures(boolean whiteTurn) {
        List<Piece> result = new ArrayList<>();
        Piece[][] board = Chessboard.getInstance().getBoard();
        for (Piece[] pieces : board) {
            for (Piece piece : pieces) {
                if (piece != null && piece.isWhite() == whiteTurn) {
                    result.add(piece);
                }
            }
        }
        return result;
    }

}
